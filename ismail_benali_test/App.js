import axios from "axios";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import {useState} from "react";

import {Table} from 'react-bootstrap';


function App() {

    const [data, setData] = useState([]);

    const [name, setName] = useState('');


    const getData = async () => {

        const res = await axios.get('http://localhost:8000/api/data/')

        console.log(res.data)
        setData(res.data)
    }

    const getDataByName = async (name) => {

        const res = await axios.get(`http://localhost:8000/api/${name}`)

        console.log(res.data)
        setData(res.data)
    }

    console.log(data);
    return (
        <div className="App">
            <div>
                <button onClick={() => getData()}>
                    Get ALL DATA
                </button>

                <div>
                    <input type={'text'} onChange={(event) => {
                        console.log(event.target.value)
                        setName(event.target.value)
                    }}/>
                    <button onClick={() => getDataByName(name)}>
                        getData by Name
                    </button>
                </div>


                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>name</th>
                        <th>time</th>
                        <th>temp</th>
                        <th>temp_min</th>
                        <th>temp_max</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        data.map((item, index) =>
                            <tr>
                                <td key={index}>{item.name}</td>
                                <td>{item.time}</td>
                                <td>{item.temp} </td>
                                <td>{item.temp_min}</td>
                                <td>{item.temp_max}</td>
                            </tr>
                        )


                    }
                    </tbody>
                </Table>
            </div>
        </div>
    );
}

export default App;
