<?php

namespace App\Http\Controllers;

use App\Models\weather;
use Illuminate\Http\Request;

class WeatherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return weather::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\weather  $weather
     * @return \Illuminate\Http\Response
     */
    public function show(string $name)
    {
        return weather::where('name',$name)->get();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\weather  $weather
     * @return \Illuminate\Http\Response
     */
    public function edit(weather $weather)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\weather  $weather
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, weather $weather)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\weather  $weather
     * @return \Illuminate\Http\Response
     */
    public function destroy(weather $weather)
    {
        //
    }
}
