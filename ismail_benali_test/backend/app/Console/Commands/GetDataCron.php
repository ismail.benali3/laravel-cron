<?php

namespace App\Console\Commands;

use App\Models\weather;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;


class GetDataCron extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       // return Command::SUCCESS;
        $this->info('getdata:Cron Command is working fine!');

        $this->getData('52.520008','13.404954') ;
        $this->getData('52.515816','13.454293') ;

        $this->info('end add weather');

    }

    private function getData($lat,$lon) {

        $response = Http::get('api.openweathermap.org/data/2.5/weather', [
            'lat' => $lat,
            'lon' => $lon,
            'appid' => 'bf65d8b174418831a16055a19f50144f'
        ]);


        weather::create([
            "name"=> $response->json('name') ,
            "time" => $response->json('timezone'),
            "lat"=>$lat,
            "lon"=>$lon,
            "temp"=>$response->json('main.temp'),
            "pressure"=>$response->json('main.pressure'),
            "temp_min"=>$response->json('main.temp_min'),
            "temp_max"=>$response->json('main.temp_max'),
            "humidity"=>$response->json('main.humidity'),
      ]);

    }

}
